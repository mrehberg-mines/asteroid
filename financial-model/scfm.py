import numpy as np
from input_functions import *
from plotting import *
import pandas as pd
import os

def baseTimeArray(var):
    timeRange = range(0,int(var['numYears']))
    df = pd.DataFrame()
    for i in timeRange:
        col_name = 'year'+str(i)
        df.loc['year',col_name] = i
    return df


def buildSpacecraft(var, df):
    df.loc['spacecraft', :] = var['craftStarting']*(1+var['craftGrowthRate'])**df.loc['year',:]
    df.loc['spacecraft', :] = df.loc['spacecraft', :].round(0)
    return df


def demandProfile(var, df):
    df.loc['demand', :] = var['demandStarting']*(1+var['demandIncreaseRate'])**df.loc['year',:]
    #df.loc['demand', :] = df.loc['demand', :].round(0)
    return df 

def launchSpacecraft(var, df):
    var['launchesPerYear'] = 1/var['launchFrequency']
    df.loc['spacecraftLaunched', :] = df.loc['spacecraft', :] * var['launchesPerYear']
    df.loc['launchedMass', :] = df.loc['spacecraftLaunched', :]*var['launchMass']
    # in k
    df.loc['launchPricePerKilo', :] = var['launchPrice']*(1- var['launchPriceDeclineRate'])**df.loc['year',:]
    df.loc['costLaunch' ,:] = df.loc['spacecraftLaunched', :]*var['launchMass']*df.loc['launchPricePerKilo', :]
    return df

def sellMaterial(var, df):
    df.loc['salePricePerKilo', :] = var['salePriceStarting']*(1-var['salePriceDeclineRate'])**df.loc['year',:]
    
    # A way to delay material based on round trips and round trip time
    trip_list = []
    for year in range(0,int(var['numYears'])):
        trip_list.append([year,0])
        spacecraftLaunched = df.loc['spacecraftLaunched', df.columns[year]]
        roundTripTime = var['roundTripTime']
        roundTrips = int(var['roundTrips'])
        for trip in range(1,roundTrips+1):
            tripYear = year + trip*roundTripTime
            if tripYear < var['numYears']:
                trips = spacecraftLaunched
                trip_list.append([int(tripYear), trips])
    trips = pd.DataFrame(trip_list, columns = ['year', 'deliveries'])
    deliveries = trips.groupby('year').sum()['deliveries'].round(0)
    df.loc['deliveryTrips'] =  deliveries.values
    df.loc['deliveredMaterial', :] = df.loc['deliveryTrips', :] * var['deliveryQuantity'] 
    df.loc['sales',:] = df.loc['salePricePerKilo', :] * df.loc['deliveredMaterial', :]
    return df


def costBuild(var, df):
    B = 1 - np.log(1/var['learningRate'])
    df.loc['costBuild', :] = df.loc['spacecraft', :]**B * var['tfu_costCer_totalCost']
    df.loc['costPerCraft', :] = df.loc['costBuild', :] / df.loc['spacecraft', :]
    # need to add in learning rate
    return df

def summary(var, df):
    df.loc['marketShare',:] = df.loc['deliveredMaterial', :]/df.loc['demand',:]
    var['totalSales'] = sum(df.loc['sales',:])
    df.loc['costs', :] = df.loc['costBuild', :] + df.loc['costLaunch', :]
    df.loc['revenue', :] = df.loc['sales', :]
    df.loc['profit', :] =  df.loc['revenue', :] - df.loc['costs', :]
    df.loc['presentValue', :]  = df.loc['profit', :]*(1-var['discountRate'])**df.loc['year', :]
    var['npv'] = sum(df.loc['presentValue', :])
    return


def runSCFM(var):
    df = baseTimeArray(var)
    df = buildSpacecraft(var, df)
    df = demandProfile(var, df)
    df = launchSpacecraft(var, df)
    df = sellMaterial(var, df)
    df = costBuild(var, df)
    summary(var, df)
    return df


