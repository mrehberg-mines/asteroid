# -*- coding: utf-8 -*-
"""
Created on Sun May 21 16:00:36 2023

@author: HelloWorld
"""
# Running cashFlow Model

import cpi
from prop_calc import calculatePropellant

global cases, saveFolder, cwd

from scfm import *

def excavationSystemMass(var):
    # this function calculates the mass of the excavation system
    # the size is related to the number of scoops, water percent, and the water goal
    if 'asteroidWaterNeeded' in var:
        regolithNeeded = var['asteroidWaterNeeded']/var['waterPercent']
    else:
        regolithNeeded = var['deliveryQuantity']/var['waterPercent']
    excavationVolume = regolithNeeded/var['excavationNumScoops']/var['asteroidDensity']
    excavationSAtoVratio = var['excavationVolumeFactor']/excavationVolume**(1/3)
    var['excavationSystemSA'] = excavationSAtoVratio*excavationVolume
    excavationMass = var['excavationMaterialDensity'] * var['excavationMaterialThickness'] * var['excavationSystemSA'] * var['excavationMassFactor']+ var['excvationMassA0']
    totalExcavationTime = var['excavationNumScoops'] * var['excavationTime']

    var.update({'excavationVolume':round(excavationVolume,2)})
    var.update({'excavationMass':round(excavationMass,0)})
    var.update({'regolithNeeded':round(regolithNeeded,0)})
    var.update({'totalExcavationTime':round(totalExcavationTime,2)})
    return 

def processingSystemMass(var):
    # This function is used to calculate the amount of energy needed to convert the asteroid material to water
    # It will then calculate the mass of the energy source and the processing equipment needed
    if var['useProcessing'] == 1:
        thermalEnergyPerKg = var['asteroidCp'] * (var['processEndTemp'] - var['processStartTemp']) / 1000 # kj/kg
        var['waterPerBatch'] = var['asteroidWaterNeeded'] / var['excavationNumScoops'] /var['processNumBatch']
        var['reactionEnergy'] = var['processEnthalpy'] * (var['waterPerBatch'] / .01802)  #kJ
        totalEnergyPerBatch = thermalEnergyPerKg * var['regolithNeeded'] / var['excavationNumScoops'] + var['reactionEnergy'] #kJ
        powerPerBatch = totalEnergyPerBatch / var['processTime'] * (1/24) * (1/60) * (1/60) #kW
        totalProcessingTime = var['processTime'] * var['excavationNumScoops'] * var['processNumBatch']  # days

        # Mass of energy supply
        solarThermalRatio = 1 - var['processSolarPanelHeatRatio']
        totalSolarThermalMass = powerPerBatch * solarThermalRatio / var['solarThermalEnergyDensity'] / var['solarThermalEfficiency'] / (1 / var['asteroidMaxSunDistance']**2)
        totalSolarPanelMass = powerPerBatch * var['processSolarPanelHeatRatio'] / var['solarPanelEnergyDensity'] / var['solarPanelEfficiency'] / (1 / var['asteroidMaxSunDistance']**2) / (1-var['solarAnnualDegradation']/100)**(var['designYears'])
        totalPowerMass = (totalSolarPanelMass + totalSolarThermalMass) * var['useProcessing']

        var['processingVolume'] = var['excavationVolume'] / var['processNumBatch'] / var['processTime']
        processSAtoVRatio = var['processVolumeFactor']/var['processingVolume']**(1/3)
        var['processSystemSA'] = processSAtoVRatio * var['processingVolume']
        processContainerMass = var['processMaterialDensity'] * var['processMaterialThickness'] * var['processSystemSA'] * var['processMassFactor']

    else:

        thermalEnergyPerKg = 0
        var['waterPerBatch'] = 0
        var['reactionEnergy'] = 0
        totalEnergyPerBatch = 0
        powerPerBatch = 0
        totalProcessingTime = 0

        # Mass of energy supply
        solarThermalRatio = 0
        totalSolarThermalMass = 0
        totalSolarPanelMass = 0
        totalPowerMass = 0

        var['processingVolume'] = 0
        processSAtoVRatio = 0
        var['processSystemSA'] = 0
        processContainerMass = 0
    
    totalProcessingMass = totalPowerMass + processContainerMass
    var.update({'totalProcessingMass':round(totalProcessingMass,2)})
    var.update({'powerPerBatch':round(powerPerBatch,3)})
    var.update({'totalPowerMass':round(totalPowerMass,2)})
    var.update({'totalEnergyPerBatch':round(totalEnergyPerBatch,2)})
    var.update({'totalProcessingTime':round(totalProcessingTime,2)})
    var.update({'processContainerMass':round(processContainerMass,2)})
    return

def spacecraftPowerCalculation(var):
    var['baseSolarPanelMass'] = (var['spacecraftBasePower'] + var['liquifierPower'] * var['liquifier']) \
                                 * var['solarPanelEnergyDensity'] / (1 / var['asteroidMaxSunDistance']**2) / (1-var['solarAnnualDegradation']/100)**(var['designYears'])
    var['batteryMass'] = var['spacecraftBasePower'] * 1000 *var['batteryTime'] / var['batteryDensity']
    
    ## Cost RD
    var['costPower1Rd'] = var['cerEPSA1'] * (var['batteryMass'] + var['baseSolarPanelMass'])
    var['costPower2Rd'] = var['cerEPSA2'] * ((var['batteryMass'] + var['baseSolarPanelMass']) * var['spacecraftBasePower'])**var['cerEPSE1']
    var['costPowerRd'] = var['costPower1Rd'] + var['costPower2Rd']
    
    ## Cost TFU
    var['costPowerTfu'] = var['cerTfuEPSA1'] * (var['batteryMass'] + var['baseSolarPanelMass'])**var['cerTfuEPSE1']
    return

def spacecraftThermalCalculation(var):
    var['spacecraftSurfaceArea'] = (var['processSystemSA'] + var['excavationSystemSA'] + var['tankSA']) * var['spacecraftSAFactor']
    var['massMLI'] = var['spacecraftSurfaceArea'] * var['mliDensity']


    var['thermalLoad'] = var['radiationPower']
    var['radiatorArea'] = var['thermalLoad'] * 1000 / (5.67e-8 * var['radiationRejectionTemperature']**4)
    var['massRadiator'] = var['radiatorArea'] * var['radiatorDensity']
    
    ## Cost
    
    return

def spacecraftCommandAndDataHandlingCalculation(var):
    var['massCDH'] = var['cdhMass']
    
    ## Cost

    return

def spacecraftStructureCalculation(var):
    ## Mass is a percent of dry mass
    # this mass is not added to the dry mass, it is just for cost calculations
    var['structureMass'] = var['dryMass']*var['structureDryMassFactor']
    ## Cost
    var['costStructureRd'] = var['cerStructureA1'] * var['structureMass']**var['cerStructureE1']
    return

def spacecraftGuidanceAndNavigationCalculation(var):
    var['massGNC'] = var['gncMass']
    
    ## Cost
    
    return

def spacecraftCommunicationsCalculation(var):
    var['massComms'] = var['commMass']
    return

def spacecraftAttitudeControlCalculation(var):
    var['massAtt'] = var['attMass']
    return

def spacecraftElectroysis(var):
    if var['electrolysis'] > 0:
        var['hydrogenProductionMass'] = var['massPropToEML1'] / 18.02 * 2.02
        var['elecEnergy'] = var['hydrogenProductionMass'] * var['elecHydrogenEnergy']
        if var['elecPower'] < (var['powerPerBatch'] * var['processSolarPanelHeatRatio']):
            var['powerElec'] = var['powerPerBatch'] * var['processSolarPanelHeatRatio']
            var['elecPowerMass'] = 0
        else:
            var['powerElec'] = var['elecPower']
            var['elecPowerMass'] = var['powerElec']  / var['solarPanelEnergyDensity'] / var['solarPanelEfficiency'] / (1 / var['asteroidMaxSunDistance']**2) / (1-var['solarAnnualDegradation']/100)**(var['designYears'])
        var['timeElectrolysis'] = var['elecEnergy'] / var['powerElec']  /24
        var['massElectrolysis'] = var['elecPowerMass'] + var['elecMass']
    else:
        var['timeElectrolysis'] = 0
        var['massElectrolysis'] = 0
        var['powerElec'] = 0
        var['elecPowerMass'] = 0
    return

def spacecraftPropulsionCalulation(var):
    if 'totalPropellant' in var:
        totalPropellant = var['totalPropellant']
    else:
        totalPropellant = 200 #kg
    var['propVolume'] = totalPropellant / var['propDensity'] / var['tankNumber']
    var['tankSA'] = var['tankVolumeFactor'] / var['propVolume']**(1/3) * var['tankNumber']
    var['massTank'] = var['tankPressurantMassFactor'] * (var['tankSA'] * var['tankMaterialThickness'] * var['propTankDensity'])
    var['massEngine'] = var['engineMass']
    return

def liquifier(var):
    if var['liquifier'] == 0:
        var['liquifierMass'] = 0
        var['liquifierPower'] = 0
        var['liquifierRDCost'] = 0
        var['liquifierTFUCost'] = 0
    return 

def waterFiltration(var):
    if var['waterFilter'] == 0:
        var['waterFilterMass'] = 0
        var['waterFilterPower'] = 0
        var['waterFilterLoss'] = 0
    
    if var['waterFilterPower'] < (var['powerPerBatch'] * var['processSolarPanelHeatRatio']):
        var['waterFilterPowerMass'] = 0
    else:
        var['waterFilterPowerMass'] = var['waterFilterPower']  / var['solarPanelEnergyDensity'] / var['solarPanelEfficiency'] / (1 / var['asteroidMaxSunDistance']**2) / (1-var['solarAnnualDegradation']/100)**(var['designYears'])
    
    var['massWaterFilter'] = var['waterFilterPowerMass'] + var['waterFilterMass']
    # water filter loss accounted for in calculateTotalPropellant
    return
 
def prospecting(var):
    if var['prospecting'] == 0:
        var['prospectingInstrumentMass'] = 0
        var['prospectingPower'] = 0

    if var['prospectingPower'] < (var['powerPerBatch'] * var['processSolarPanelHeatRatio']):
        var['prospectingPowerMass'] = 0
    else:
        var['prospectingPowerMass'] = var['prospectingPower']  / var['solarPanelEnergyDensity'] / var['solarPanelEfficiency'] / (1 / var['asteroidMaxSunDistance']**2) / (1-var['solarAnnualDegradation']/100)**(var['designYears'])
    return


def summationVariables(var):
    var['totalPayloadMass'] = var['totalProcessingMass'] + var['excavationMass']
    var['totalStayDays'] = var['totalProcessingTime'] + var['excavationTime']
    var['dryMass'] = var['totalProcessingMass'] + var['excavationMass'] + var['baseSolarPanelMass'] +\
        var['cdhMass'] +  var['massRadiator'] + var['massMLI'] + var['massGNC'] + var['massEngine'] +\
            var['massTank'] + var['commMass'] + var['massAtt'] + var['batteryMass'] + var['massElectrolysis'] +\
                var['liquifierMass'] + var['massWaterFilter'] + var['prospectingInstrumentMass'] + var['prospectingPowerMass'] +\
                    00
    var['dryMass']  = var['dryMass'] * (1 + var['structureDryMassFactor'] + var['dryMassMargin'])
    var['launchMass'] = var['dryMass'] + var['launchedPropellant']
    var['percentPropSold'] =  var['deliveryQuantity'] / var['asteroidWaterNeeded']
    return

def simOrder(var):
    calculateTotalPropellant(var)
    excavationSystemMass(var)
    processingSystemMass(var)
    spacecraftPowerCalculation(var)
    spacecraftPropulsionCalulation(var)
    spacecraftGuidanceAndNavigationCalculation(var)
    spacecraftThermalCalculation(var)
    spacecraftAttitudeControlCalculation(var)
    spacecraftCommunicationsCalculation(var)
    spacecraftCommandAndDataHandlingCalculation(var)
    spacecraftElectroysis(var)
    liquifier(var)
    waterFiltration(var)
    prospecting(var)
    summationVariables(var)
    spacecraftStructureCalculation(var)
    return




def calculateTotalPropellant(var):
    isp = var['engineISP']
    dV1 = var['deltaVtoAsteroid']
    dV2 = var['deltaVtoEML1']
    v_e = isp * 9.81 / 1000 
    if 'dryMass' not in var:
         dryMass = var['dryMassGuess'] * var['spacecraftDryMassFactor'] * (1 + var['dryMassMargin'])
    else: 
        dryMass = var['dryMass']
    
    Ap = var['asteroidPropellantRatio']
    massProp1Guess = 100
    massProp2Guess = 100
    massAsteroid = var['deliveryQuantity']
    maxIterations = 100
    convergence = 0.01
    for i in range(maxIterations):
        massProp1, massProp2, diff_dV1, diff_dV2 = calculatePropellant(v_e, dryMass, dV1, dV2, massAsteroid, Ap, massProp1Guess, massProp2Guess)
        if abs(diff_dV1) +abs(diff_dV2) > convergence:
            massProp1Guess = massProp1Guess*(1+diff_dV1*0.2)
            massProp2Guess = massProp2Guess*(1+diff_dV2*0.2)
        else: 
            break
    var['totalPropellant'] = massProp1 + massProp2 
    var['launchedPropellant'] = massProp1 + (1- var['asteroidPropellantRatio']) * massProp2
    var['asteroidWaterNeeded'] = var['deliveryQuantity'] + massProp2 *(var['asteroidPropellantRatio']) + var['waterFilterLoss']*var['waterFilter'] + var['launchedPropellant']
    
    var['netPropellant'] = var['deliveryQuantity'] #- var['launchedPropellant']
    
    var.update({'massPropToAsteroid':round(massProp1,0)})
    var.update({'massPropToEML1':round(massProp2,0)})
    return


def calculateSpacecraftCost(var):

    var['rdte_costCer_spacecraft'] = var['dryMass'] * var['cerDryMassA1'] + var['costStructureRd'] + var['costPowerRd'] + var['liquifierRDCost']
    var['rdte_costCer_iatX1'] = var['rdte_costCer_spacecraft'] 
    var['rdte_costCer_iat'] = var['cerIATA0'] + var['cerIATA1'] * var['rdte_costCer_iatX1']
    # same X1 as IAT, integration assembly and test
    var['rdte_costCer_programLevelCostCER'] = var['cerProgramA1'] * var['rdte_costCer_iatX1'] **(var['cerProgramE1'])
    
    var['rdte_costCer_totalCost'] = var['rdte_costCer_programLevelCostCER'] + var['rdte_costCer_iat'] + var['rdte_costCer_spacecraft']
    var['rdte_costCer_totalCost'] = var['rdte_costCer_totalCost'] / var['commercialization']
    
    #Theoretical First Unit Cost
    var['tfu_costCer_spacecraft'] = var['dryMass'] * var['cerTFUDryMassA1']
    var['tfu_costCer_iatX1'] = var['dryMass'] 
    var['tfu_costCer_iat'] = var['cerTFUIATA1'] * var['tfu_costCer_iatX1']
    
    var['tfu_costCer_programX1'] = var['tfu_costCer_spacecraft'] + var['tfu_costCer_iat']
    var['tfu_costCer_programLevelCostCER'] = var['cerTFUProgramA1'] * var['tfu_costCer_programX1']
    
    var['tfu_costCer_totalCost'] = var['tfu_costCer_programLevelCostCER'] + var['tfu_costCer_iat'] + var['tfu_costCer_spacecraft'] + var['costPowerTfu'] +var['liquifierTFUCost']
    var['tfu_costCer_totalCost'] = var['tfu_costCer_totalCost'] * var['cerTFUTRLFactor'] / var['commercialization']
    
    var['totalCost'] = var['tfu_costCer_totalCost'] + var['rdte_costCer_totalCost']
    
    #convert to 2023 $
    var['rdte_costCer_totalCost'] = round(cpi.inflate(var['rdte_costCer_totalCost'],2000,to=2022),0)
    var['rd_per_kg'] = var['rdte_costCer_totalCost']/var['dryMass']
    var['tfu_costCer_totalCost'] =round(cpi.inflate(var['tfu_costCer_totalCost'], 2000, to=2022),0)
    var['tfu_per_kg'] = var['tfu_costCer_totalCost']/var['dryMass'] 
    var['totalCost'] =round(cpi.inflate(var['totalCost'], 2000, to=2022) ,0)
    var['totalCost_per_kg'] = var['totalCost']/var['dryMass']
    var['costPerKgWater'] = var['totalCost'] / var['deliveryQuantity']
    return


def costMetrics(var):
    var['launchedMass'] = (var['dryMass'] + var['launchedPropellant'])
    var['launchCost'] = var['launchedMass']*var['launchPrice']
    var['totalCostPlusLaunch'] = var['totalCost'] + var['launchCost']
    var['costLaunchAndTFU'] = var['launchCost'] + var['tfu_costCer_totalCost']
    var['soldProp'] = (var['roundTrips'])*var['deliveryQuantity'] + var['massPropToAsteroid']*var['asteroidPropellantRatio']
    var['recoverySalePrice'] = (var['costLaunchAndTFU'] + var['rdte_costCer_totalCost']) / var['soldProp']
    return


    
def runSim(var):
    # these variables determine the maximum number of iterations and convergence minimum for the dry mass calculations
    maxIterations = 1000
    convergence = 1 #kg
    propGuess = 200
    for iter in range(maxIterations):
        if iter == 0:
            simOrder(var)
            propCheck = abs(propGuess - var['totalPropellant'])
        else:
            if propCheck > convergence:
                propGuess = var['totalPropellant']
                simOrder(var)
            else:
                break
    calculateSpacecraftCost(var)
    costMetrics(var)
    # 
    return



def singleVariableRange(inputs,inputVar,varRangeMin,varRangeMax,step, numYears):
    outputs = pd.DataFrame()
    outDict = dict()
    factor = 1/step
    for a in range(int(varRangeMin * factor), int(varRangeMax * factor)):
        a_in = a / factor
        var = defineInputData(inputs, force = 'Yes')
        var[inputVar] = a_in
        runSim(var)
        var['numYears'] = numYears
        var['id'] = a_in
        df = runSCFM(var)
        outDict.update({a_in:df}) 
        outputs = pd.concat([outputs,pd.DataFrame.from_dict(var, orient='index').T])    
    return outputs, outDict