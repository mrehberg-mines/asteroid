# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 17:02:25 2023

@author: mrehberg
"""
import math
import pandas as pd
import matplotlib.pyplot as plt
 
def plottingMultiCaseOutputCorellations(df, x, y, varied, varName,Ap, ylim=None, xlim=None, saveFile=None):
    fig, ax = plt.subplots(figsize=(10,6))
    plt.rcParams.update({'font.size': 12})
    for var in varied:
        output_case = df[df[varName]==var]
        ax.plot(output_case[x], output_case[y], 'o-', label=f'{varName}={var}/s')
    ax.set_xlabel(x + ', km/s')
    ax.set_ylabel(y + ', kg')
    ax.legend()
    if ylim!=None:
        ax.set_ylim(ylim)
    if xlim!=None:
        ax.set_xlim(xlim)
    #ax.set_title(f'Total Propellant Needed when Utilizing {Ap}% from the Asteroid')
    # ax.plot(np.unique(outputs[x]), np.poly1d(np.polyfit(outputs[x], outputs[y], 1))(np.unique(outputs[x])), color='r')
    # if saveFile==True:
    #     plt.savefig(saveFolder + f'{x}_vs_{y}.png')
    return




def calculatePropellant(v_e,m_dry,dV1,dV2,m_asteroid,Ap,m_guess_1, m_guess_2):
    m_prop_1 = m_guess_1
    m_prop_2 = m_guess_2
    
    dV1_calc = v_e * math.log((m_dry + m_prop_1 + (1-Ap)*m_prop_2)/(m_dry + (1-Ap)*m_prop_2))
    dV2_calc = v_e * math.log((m_dry + m_prop_2 + m_asteroid)/(m_dry + m_asteroid))
    
    diff_dV1 = dV1 - dV1_calc
    diff_dV2 = dV2 - dV2_calc
    return m_prop_1, m_prop_2, diff_dV1, diff_dV2



# isp_range = range(300,1100,100)


# m_asteroid=1000

# # This is the amount of propellant harvested from the asteroid
# # 1 corresponds to 100% propellant of the return prop comes from the asteroid
# Ap=0
# m_dry = 250

# # # # 
# max_iter = 10000
# results = []
# for isp in isp_range:
#     ISP = isp
#     v_e = ISP * 9.81 / 1000 
#     m_guess_1=10
#     m_guess_2=10
#     for a in range(5,60,2):
#         dv = a/10
#         dV2 = dv - 0.1
#         for i in range(max_iter):
#             try:
#                 m_prop_1, m_prop_2, diff_dV1, diff_dV2 = calculatePropellant(v_e,m_dry,dv,dV2,m_asteroid,Ap,m_guess_1, m_guess_2)
#                 if abs(diff_dV1) + abs(diff_dV2) > 0.01:
#                     m_guess_1 = m_guess_1*(1+diff_dV1*.2)
#                     m_guess_2 = m_guess_2*(1+diff_dV2*.2)
#                 else: 
#                     results.append((dv,isp, m_prop_1, m_prop_2))
#                     m_guess_1=m_prop_1*0.5
#                     m_guess_2=m_prop_2*0.5
#                     break
#             except:
#                 break


# df = pd.DataFrame(results, columns=['One Way DeltaV', 'ISP','propToAsteroid', 'propToEML'])
# df['Required Propellant'] = df['propToAsteroid'] + df['propToEML']*(1-Ap)
# plottingMultiCaseOutputCorellations(df, 'One Way DeltaV', 'Required Propellant', isp_range, 'ISP', Ap*100, ylim=[0,2500], xlim=None, saveFile=None)

