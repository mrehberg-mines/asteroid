# -*- coding: utf-8 -*-
"""
Created on Sun May 21 15:59:00 2023

@author: HelloWorld
"""

#graphing
# Running cashFlow Model
import pandas as pd
import matplotlib.pyplot as plt
from input_functions import defineInputData
from scamm import runSim
global cases, saveFolder, cwd

from scfm import runSCFM

def addlabels(sensitivities, df_inputs, var):
    for i in range(len(sensitivities)):
        if i > 12:
            break
        y = sensitivities[var][i]
        varName = sensitivities['varName'][i]
        value = df_inputs[df_inputs['varName']==varName][var].values[0]
        # plt.text(y, i, value, ha = 'center', fontsize=11, 
        #          Bbox = dict(facecolor = 'white', alpha =.6))

def tornado(outputVar, inputs, maxEffect=15, saveFile=None, units= '', maxRound=None, saveFolder=None):
    plt.rcParams.update({'font.size': 16})
    #remove categorical variables for now
    df_inputs = inputs[inputs['varType']=='numerical'].reset_index(drop=True)
    # create a dataframe to house the sensitivity values
    senseCols = ['varMin', 'p10', 'p50', 'p90', 'varMax']
    sensitivities = pd.DataFrame(columns=['varName'] + senseCols)
    for row in range(len(df_inputs)):
        # run though all of the calculations for the min, p10, p50, p90, and max value
        varName = df_inputs.loc[row, 'varName']
        sensitivities.loc[row,'varName'] =  varName
        sensitivities.loc[row,'label'] =  df_inputs.loc[row, 'Name']
        for sense in senseCols:
            #copy running statements from the trial loop
            # could put these in a for loop for single running eventually
            varTest = defineInputData(inputs, force=sense, forceVar=varName)
            runSim(varTest)
            df = runSCFM(varTest)
            sensitivities.loc[row,sense] = float(varTest[outputVar])
    
    sensitivities['sum'] = 0
    for col in senseCols:
        if col not in ['varName','p50']:
            sensitivities[col] = sensitivities[col] - sensitivities['p50']
            sensitivities['sum']+=abs(sensitivities[col])
    

    # ##ploting
    sensitivities = sensitivities.sort_values(by=['sum'], ascending=False).reset_index(drop=True)
    sensitivities = sensitivities[0:maxEffect]
    
    fig, ax = plt.subplots(figsize=(10,0.75*len(sensitivities)))
    ax.barh(sensitivities['label'], sensitivities['varMin'] , align='center', color='firebrick', label='varMin')
    addlabels(sensitivities, df_inputs, 'varMin')
    ax.barh(sensitivities['label'], sensitivities['p10'] , align='center', color='palevioletred', label='p10')
    addlabels(sensitivities, df_inputs, 'p10')
    ax.barh(sensitivities['label'], sensitivities['varMax'] , align='center', color='limegreen',label='varMax')
    addlabels(sensitivities, df_inputs, 'varMax')
    ax.barh(sensitivities['label'], sensitivities['p90'] , align='center', color='yellowgreen', label='p90')
    addlabels(sensitivities, df_inputs, 'p90')
    plt.axvline(x=0, color='black', linestyle='--')
    ax.set_yticks(sensitivities['label'].to_list())
    plt.legend()
    ax.invert_yaxis()  # labels read top-to-bottom
    ax.set_xlabel(f'Change to {outputVar}')
    medianVal = sensitivities['p50'].median()
    if maxRound!=None:
        medianVal = round(medianVal, int(maxRound))
    medianVal = str(medianVal)
    ax.set_title(f'Sensitivity plot for {outputVar} \n The nominal case is {medianVal} {units}')
    if saveFile == True:
        plt.savefig(saveFolder + f'{outputVar}.png')
    plt.show()
    return

def plottingOutputCorellations(outputs, x, y, ylim=None, xlim=None, saveFile=None, saveFolder=None):
    fig, ax = plt.subplots(figsize=(10,6))
    ax.scatter(outputs[x], outputs[y])
    ax.set_xlabel(x)
    ax.set_ylabel(y)
    if ylim!=None:
        ax.set_ylim(ylim)
    if xlim!=None:
        ax.set_xlim(xlim)
    # ax.plot(np.unique(outputs[x]), np.poly1d(np.polyfit(outputs[x], outputs[y], 1))(np.unique(outputs[x])), color='r')
    if saveFile==True:
        plt.savefig(saveFolder + f'{x}_vs_{y}.png')
    return

def plottingMultiCaseOutputCorellations(outputs, x, y, cases, ylim=None, xlim=None, saveFile=None, saveFolder=None):
    fig, ax = plt.subplots(figsize=(10,6))
    for case in cases:
        output_case = outputs[case]
        ax.plot(output_case[x], output_case[y], 'o-', label=case)
    ax.set_xlabel(x)
    ax.set_ylabel(y)
    ax.legend()
    if ylim!=None:
        ax.set_ylim(ylim)
    if xlim!=None:
        ax.set_xlim(xlim)
    # ax.set_title(casesTitle)
    # ax.plot(np.unique(outputs[x]), np.poly1d(np.polyfit(outputs[x], outputs[y], 1))(np.unique(outputs[x])), color='r')
    if saveFile==True:
        plt.savefig(saveFolder + f'{x}_vs_{y}.png')
    return


def getpValues(outputs, outVars):
    pVals = pd.DataFrame()
    for outVar in outVars:
        temp = {}
        temp['Name'] = outVar
        temp['varName'] = outVar
        temp['varType'] = 'numerical'
        temp['units'] = ''
        temp['use'] = 'randomize'
        temp['varMin'] = min(outputs[outVar]).round(1)
        temp['p10'] = outputs[outVar].quantile(0.1).round(1)
        temp['p50'] = outputs[outVar].quantile(0.5).round(1)
        temp['p90'] = outputs[outVar].quantile(0.9).round(1)
        temp['varMax'] = max(outputs[outVar]).round(1)
        temp['numberDecimals'] = 1
        temp['std'] = outputs[outVar].std()
        print(outVar, temp['p50'], '±', temp['std'])
        df_temp = pd.DataFrame.from_dict(temp, orient='index').T
        pVals = pd.concat([pVals, df_temp])
    return pVals


## Some stealth boxplot code
def boxplot(outputs, outVar, label, title, ylim=None, saveFolder=None):
    fig, axs = plt.subplots(1, 1, figsize=(5,5))
    
    plt.rcParams.update({'font.size': 10})
    
    # basic plot
    axs.boxplot(outputs[outVar],1)
    if ylim!=None:
        axs.set_ylim(ylim)
    axs.set_ylabel(label)
    axs.set_xlabel('')
    axs.set_title(title)
    return