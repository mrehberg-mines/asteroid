# Running cashFlow Model
import pandas as pd
import random
from scipy.stats import norm
import cpi
import matplotlib.pyplot as plt

import os

global cases, saveFolder, cwd

from prop_calc import calculatePropellant
from input_functions import *
from plotting import *
from scamm import *
from scfm import *


def main(cases, running, diffCases):
    ## Run a monte Carlo
    if running == 'MonteCarlo':
        inputs = readInputs(cases, commonFile='common-inputs.csv')
        outputs = pd.DataFrame()
        numTrials = 50
        outDict = dict()
        for trial in range(numTrials):
            var = defineInputData(inputs)
            runSim(var)
            var['id'] = trial
            df = runSCFM(var)
            outDict.update({trial:df}) 
            outputs = pd.concat([outputs,pd.DataFrame.from_dict(var, orient='index').T])
        # outputs.to_csv(cwd + '\\outputs\\simData.csv', float_format='%.3f')
        plottingOutputCorellations(outputs, y='npv', x='deliveryQuantity', saveFile=True, saveFolder=saveFolder)
        return outputs, outDict
    
    ### run a single variable run
    if running == 'Range':
        inputs = readInputs(cases, commonFile='common-inputs.csv')
        minRange = 100
        maxRange = 3100
        step = 100
        changeVar = 'deliveryQuantity'
        outputs, outDict = singleVariableRange(inputs,changeVar, minRange, maxRange, step, numYears)
        plottingOutputCorellations(outputs, y='npv', x=changeVar, xlim=[minRange,maxRange], saveFile=True, saveFolder=saveFolder)
        return outputs, outDict
    

    if running == 'diff_cases':
        minRange = 100
        maxRange = 3100
        step = 100
        changeVar = 'deliveryQuantity'
        casesOutput = dict()
        for case in diffCases:
            cases.append(case)
            inputs = readInputs(cases, commonFile='common-inputs.csv')
            cases.remove(case)
            outputs, outDict = singleVariableRange(inputs, changeVar, minRange, maxRange, step, numYears)
            casesOutput.update({case:outputs})

        plottingMultiCaseOutputCorellations(casesOutput, changeVar, 'npv', diffCases, ylim=None, xlim=None, saveFile=True, saveFolder=saveFolder)      
        return casesOutput


    ## Tornado Plots
    if running == 'Tornado':
        inputs = readInputs(cases, commonFile='common-inputs.csv')
        tornado('npv', inputs, maxEffect=13, maxRound=0, units='k$', saveFile=False)
        return 'none', 'none'

if __name__ =='__main__':
    cwd = os.path.dirname(os.path.realpath(__file__))
    # Can specify as many as desired with the most important case specified last
    # because any duplicate values resort to the last specified value
    # If you do not want any cases, specify 'base'
    cases = ['base']
    #this only does something if running is diff-cases
    diffCases = ['case-engine-steam','case-engine-GH2GO2', 'case-engine-LH2LO2']
    running = 'Tornado'
    # this is the type of analysis to run
    print(cases)
    print(running)
    saveFolder= cwd + f'\\figures\\{running}\\'
    outputs, outDict = main(cases, running, diffCases)
