# -*- coding: utf-8 -*-
"""
Created on Sun May 21 15:56:59 2023

@author: HelloWorld
"""

## Inputs
# Running cashFlow Model
import pandas as pd
import random
from scipy.stats import norm
global cases, saveFolder, cwd

def readInputs(cases, commonFile):
    # read common inputs
    common = pd.read_csv(commonFile)
    common = processInputs(common)
    # read input file
    combine = common.copy()
    for case in cases:
        input1 = pd.read_csv(case+'/inputs.csv')
        input1 = processInputs(input1)
        combine = pd.concat([combine, input1])
    # combine these DFs, and keep the specific inputs
    combine = combine.drop_duplicates(subset=['varName'], keep='last').reset_index(drop=True)
    return combine

def processInputs(input1):
    input1=input1[input1['Name'].str[0]!='#'].reset_index(drop=True)
    # fill NaN of numeric columns
    numericCols=['varMin', 'p10','p50','p90','varMax','numberDecimals']
    input1[numericCols]=input1[numericCols].fillna(-1)
    for col in numericCols:
        input1[col] = input1[col].replace('\\u200b','')
    # Assign data types
    dataTypeDict={'numberDecimals': int,
                'varMin': float,
                'p10': float,
                'p50': float,
                'p90': float,
                'varMax': float}
    input1= input1.astype(dataTypeDict)
    return input1

def weibullDistribution(input_list):
    [p10, p50, p90, varMin, varMax] = input_list
    number_of_samples = 1
    b_mark = ((float(p90) - float(p50)) / (float(p50) - float(p10)))
    for i in range(number_of_samples):
        rand_numb = random.random()
        factor = norm.ppf(rand_numb, 0, 0.780304146072379)
        if 0.9999 < b_mark < 1.0001: 
            sample = p50 + (p90 - p50) * factor
        else:
            sample = p50 + (p90 - p50)*((b_mark**factor - 1)/(b_mark - 1))
        sample=min(max(sample, varMin), varMax)
        #samples.append(sample)
    return sample

def defineInputData(inputs, force=None, forceVar = None):
    #check for duplicate variable names
    if max(inputs['varName'].value_counts())>1:
        raise ValueError("There are duplicate varNames")
    # Create an input dataframe for number of years specified
    var_dict=dict()
    for row in range(len(inputs)):
        varType = inputs.loc[row,'varType']
        varName=inputs.loc[row, 'varName']
        if force != None:
            if forceVar == varName:
                use = force
            else: 
                use = 'p50'
        else:
            use=inputs.loc[row,'use']
        # Generate a random variable within the bounds
        if varType == 'numerical':
            if use == 'randomize':
                value = weibullDistribution(inputs.loc[row, ['p10', 'p50', 'p90', 'varMin', 'varMax']])
            else:
                value = inputs.loc[row, use]
            value = round(value, inputs.loc[row, 'numberDecimals'])
        
        # generate a random value from the list provided
        elif varType == 'categorical':
            if use == 'randomize':
                randomList = inputs.loc[row,'categorical'].split(',')
                value = random.choice(randomList)
            else:
                value = inputs.loc[row, use]
                
        var_dict.update({varName: value})
    return var_dict